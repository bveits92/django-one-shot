from django.urls import path
from todos.views import (
    todos,
    list_detail,
    create_todolist,
    todo_list_update,
    todo_list_delete,
    create_todoitem,
    edit_todoitem
)

urlpatterns = [
    path('todos/', todos, name='todo_list_list'),
    path('todos/<int:id>/', list_detail, name='todo_list_detail'),
    path('todos/create/', create_todolist, name='todo_list_create'),
    path('todos/<int:id>/edit/', todo_list_update, name='todo_list_update'),
    path('todos/<int:id>/delete/', todo_list_delete, name='todo_list_delete'),
    path('todos/items/create/', create_todoitem, name='todo_item_create'),
    path('todos/items/<int:id>/edit/', edit_todoitem, name='todo_item_update'),
]
