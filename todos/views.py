from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from .forms import TodoListForm, TodoItemForm


# Create your views here.
def todos(request):
    lists = TodoList.objects.all()
    context = {
        'lists': lists,
    }
    return render(request, 'todos/index.html', context)



def list_detail(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    context = {
        "todo_list": todo_list
    }
    return render(request, 'todos/list.html', context)


def create_todolist(request):
    if request.method == 'POST':
        form = TodoListForm(request.POST)
        if form.is_valid():
            todolist = form.save()
            return redirect('todo_list_detail', todolist.id)
    else:
        form = TodoListForm()
    context = {
        'form': form,
    }
    return render(request, 'todos/create.html', context)


def todo_list_update(request, id):
    todolist = TodoList.objects.get(id=id)
    if request.method == 'POST':
        form = TodoListForm(request.POST, instance=todolist)
        if form.is_valid():
            todolist = form.save()
            return redirect('todo_list_detail', id=todolist.id)
    else:
        form = TodoListForm(instance=todolist)
    context = {
        'form': form,
    }
    return render(request, 'todos/edit.html', context)


def todo_list_delete(request, id):
    todolist = TodoList.objects.get(id=id)
    if request.method == 'POST':
        todolist.delete()
        return redirect('todo_list_list')
    context = {
        'todolist': todolist
    }
    return render(request, 'todos/delete.html', context)


def create_todoitem(request):
    if request.method == 'POST':
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect('todo_list_detail', id=item.list.id)
    else:
        form = TodoItemForm()
    context = {
        'form': form,
    }
    return render(request, 'todos/create_item.html', context)


def edit_todoitem(request, id):
    item = TodoItem.objects.get(id=id)

    if request.method == 'POST':
        form = TodoItemForm(request.POST, instance=item)
        if form.is_valid():
            item = form.save()
            return redirect('todo_list_detail', id=item.list.id)
    else:
        form = TodoItemForm(instance=item)

    context = {
        'form': form,
        'item': item,
    }
    return render(request, 'todos/edit_item.html', context)
